class DeviceWorld extends Area {
  Server server;
  Client[] devices;
  int[][] activeGlobal;
  XML config;
  String[][] globals;
  String[][] clientResolutions;
  
  DeviceWorld(Server server) {
    this.server = server;
    config = loadXML("config.xml");
    this.devices = new Client[int(config.getChild("max_clients").getContent())];
    this.constructAreas();
    this.fillAreas();
  }
  
  /* Create globals array */
  void constructAreas() {
    //Set globals array from the xml
    XML[] rows = config.getChild("globals").getChildren("row");
    int x = 0;
    int y = rows.length;
    
    for(int i = 0; i < y; i++) {
      int areas = rows[i].getChildren("area").length;
      if (areas > x) {
        x = areas;
      }
    }
    
    globals = new String[y][x];
    clientResolutions = new String[config.getChild("max_clients").getIntContent()][3];
  }
  
  /* Fill all rows from the globals array with client IPs */
  void fillAreas() {
    XML[] rows = config.getChild("globals").getChildren("row");
    XML[] areas;
    boolean serverPlaced = false;
    
    for(int i = 0; i < rows.length; i++) {
      areas = rows[i].getChildren("area");
      
      for(int j = 0; j < areas.length; j++) {
        this.globals[i][j] = areas[j].getContent();
        
        if ( ! serverPlaced && areas[j].getContent().equals("127.0.0.1")) {
          this.activeGlobal = new int[][] {{i}, {j}};
          this.globals[i][j] = this.server.toString();
          serverPlaced = true;
        }
      }
    }
  }
  
  /* Add new connected client to the clients list */
  void updateClients(Client client) {
    // Get next open position to add client in globals
    for (int i = 0; i < this.devices.length; i++) {    
      if (this.devices[i] == null) {
        this.devices[i] = client;
        this.clientResolutions[i] = new String[] {client.toString(), "set", "set"};
        break;
      }
    }
    
    int[] location = {-1, -1};
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    int index = 0;
    for (int i = 0; i < column_length; i++) {
      for (int j = 0; j < row_length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.ip()) && location[0] == -1) {
            location[0] = i;
            location[1] = j;
          }
        }
        
        if (client.toString() == this.globals[i][j]) {
          return;
        }
      }
    }
    
    if (location[0] != -1) {
      this.globals[location[0]][location[1]] = client.toString();
      
      int i = location[0];
      int j = location[1];
      
      //If walls are set
      if (this.simulation.wall != null) {
        JSONAction message = new JSONAction().setClass("Wall").addMethod("delete");
        JSONAction message_new_client = new JSONAction().setClass("Wall").addMethod("delete");
        
        //Delete walls
        if (this.simulation.wall.length > 0) {
          for (int a = j; a > 0; a--) {
            if (a-1 >= 0) {
              if (this.globals[i][a-1] != null && this.globals[i][a-1].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("left");
                else {
                  message_new_client.addArg("delete", "name", "String", "left");
                }
                
                index = this.indexOfClient(this.globals[i][a-1]);
                if (index == -1)
                  this.simulation.deleteWall("right");
                else {
                  message.addArg("delete", "name", "String", "right");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = j; a < row_length-1; a++) {
            if (a+1 <= row_length) {
              if (this.globals[i][a+1] != null && this.globals[i][a+1].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("right");
                else {
                  message_new_client.addArg("delete", "name", "String", "right");
                }
                
                index = this.indexOfClient(this.globals[i][a+1]);
                if (index == -1)
                  this.simulation.deleteWall("left");
                else {
                  message.addArg("delete", "name", "String", "left");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = i; a > 0; a--) {
            if (a-1 >= 0) {
              if (this.globals[a-1][j] != null && this.globals[a-1][j].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("up");
                else {
                  message_new_client.addArg("delete", "name", "String", "up");
                }
                
                index = this.indexOfClient(this.globals[a-1][j]);
                if (index == -1)
                  this.simulation.deleteWall("down");
                else {
                  message.addArg("delete", "name", "String", "down");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = i; a < column_length-1; a++) {
            if (a+1 <= column_length) {
              if (this.globals[a+1][j] != null && this.globals[a+1][j].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("down");
                else {
                  message_new_client.addArg("delete", "name", "String", "down");
                }
                
                index = this.indexOfClient(this.globals[a+1][j]);
                if (index == -1)
                  this.simulation.deleteWall("up");
                else {
                  message.addArg("delete", "name", "String", "up");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }

          if (message_new_client.hasArgs("delete")) {
            index = this.indexOfClient(client.toString());
            this.devices[index].write(message_new_client.getJSON());
          }
        }
      }
    }
  }
  
  void removeInactive(Client client) {
    for (int i = 0; i < this.devices.length; i++) {
      if (client == this.devices[i]) {
        this.devices[i] = null;
        this.clientResolutions[i] = new String[3];
      }
    }
    
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    int index = 0;
    JSONAction message = new JSONAction();
    for (int i = 0; i < this.globals.length; i++) {
      for (int j = 0; j < this.globals[i].length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.toString())) {
            this.globals[i][j] = client.ip();
            
            //Create walls
            if (this.simulation.wall != null && this.simulation.wall.length > 0) {
              for (int a = j; a > 0; a--) {
                if (a-1 >= 0) {
                  if (this.globals[i][a-1] != null && this.globals[i][a-1].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "right");
                    index = this.indexOfClient(this.globals[i][a-1]);
                    if (index == -1)
                      this.simulation.createWall("right");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = j; a < row_length-1; a++) {
                if (a+1 <= row_length) {
                  if (this.globals[i][a+1] != null && this.globals[i][a+1].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "left");
                    index = this.indexOfClient(this.globals[i][a+1]);
                    if (index == -1)
                      this.simulation.createWall("left");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = i; a > 0; a--) {
                if (a-1 >= 0) {
                  if (this.globals[a-1][j] != null && this.globals[a-1][j].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "down");
                    index = this.indexOfClient(this.globals[a-1][j]);
                    if (index == -1)
                      this.simulation.createWall("down");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = i; a < column_length-1; a++) {
                if (a+1 <= column_length) {
                  if (this.globals[a+1][j] != null && this.globals[a+1][j].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "up");
                    index = this.indexOfClient(this.globals[a+1][j]);
                    if (index == -1)
                      this.simulation.createWall("up");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
            }
            
            return;
          }
        }
      }
    }
  }
  
  void doClientAction(Device device) {
    if (device.json == null)
      return;
    
    Set Classes = device.json.keys();
    for(Object classname : Classes) {
      JSONObject methods = device.json.getJSONObject(classname.toString()).getJSONObject("methods");
      Set method_indexes = methods.keys();
      
      for (Object method_index : method_indexes) {
        JSONObject method = methods.getJSONObject(method_index.toString());
        JSONObject args = method.getJSONObject("args");
        Class[] params = new Class[args.size()];
        Object[] values = new Object[args.size()];
        
        for (int i = 0; i < args.size(); i++) {
          JSONObject argument = args.getJSONObject(String.valueOf(i));
          
          switch(argument.getString("type")) {
            case "String":
              params[i] = String.class;
              values[i] = argument.getString("value");
              break;
            case "int": case "Integer":
              params[i] = int.class;
              values[i] = argument.getInt("value");
              break;
            case "float": case "Float":
              params[i] = float.class;
              values[i] = argument.getFloat("value");
              break;
            case "double": case "Double":
              params[i] = double.class;
              values[i] = argument.getDouble("value");
              break;
            case "boolean": case "Boolean":
              params[i] = boolean.class;
              values[i] = argument.getBoolean("value");
              break;
            case "char": case "Character":
              params[i] = char.class;
              values[i] = argument.getString("value");
              break;
          }
        }
        
        if (method.getString("method").equals("setResolution")) {
          for (int i = 0; i < this.clientResolutions.length; i++) {
            if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(device.last_client)) {
              this.clientResolutions[i][0] = values[0].toString();
            }
          }
        }
        
        if (method.getString("method").equals("check")) {
          int column_length = this.globals.length;
          int row_length = this.globals[0].length;
          int i = 0, j = 0;
          boolean found = false;
          
          try {
            for (i = 0; i < column_length; i++) {
              for (j = 0; j < row_length; j++) {
                if (this.globals[i][j] != null && this.globals[i][j].equals(device.last_client)) {
                  found = true;
                  break;
                } 
              }
              if (found == true)
                break;
            }


            if (values[0].equals("bottom") && i-1 >= 0 && !this.globals[i-1][j].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i-1][j]);
              println("top " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("top") && i+1 < this.globals.length && !this.globals[i+1][j].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i+1][j]);
              println("bot " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("right") && j-1 >= 0 && !this.globals[i][j-1].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i][j-1]);
              println("left " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("left") && j+1 < this.globals[0].length && !this.globals[i][j+1].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i][j+1]);
              println("right " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else {
              device.json = null;
              return;
            }
          }
          catch (Exception e) {
            println("exception: " + e.getCause());
          }
        }
        
        if (method.getString("method").equals("update")) {
          int column_length = this.globals.length;
          int row_length = this.globals[0].length;
          int i = 0, j = 0;
          boolean found = false;
          
          for (i = 0; i < column_length; i++) {
            for (j = 0; j < row_length; j++) {
              if (this.globals[i][j] != null && this.globals[i][j].equals(device.last_client)) {
                found = true;
                break;
              } 
            }
            if (found == true)
              break;
          }
          
          if (values[10].toString().equals("up") && i > 0 && this.globals[i-1][j] != null) {
            int index = this.indexOfClient(this.globals[i-1][j]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int xaxis = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), client_name);
              String string_json = device.json.toString().replace(
                 "\"name\": \"xpos\",\"value\": \"" + values[8].toString() + "\",",
                 "\"name\": \"xpos\",\"value\": \"" + xaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }  
          else if (values[10].toString().equals("down") && i < column_length && this.globals[i+1][j] != null) {
            int index = this.indexOfClient(this.globals[i+1][j]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int xaxis = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"xpos\",\"value\": \"" + values[8].toString() + "\",",
                 "\"name\": \"xpos\",\"value\": \"" + xaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }  
          else if (values[10].toString().equals("left") && j > 0 && this.globals[i][j-1] != null) {
            int index = this.indexOfClient(this.globals[i][j-1]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int yaxis = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"ypos\",\"value\": \"" + values[9].toString() + "\",",
                 "\"name\": \"ypos\",\"value\": \"" + yaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }
          else if (values[10].toString().equals("right") && j < row_length && this.globals[i][j+1] != null) {
            int index = this.indexOfClient(this.globals[i][j+1]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int yaxis = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"ypos\",\"value\": \"" + values[9].toString() + "\",",
                 "\"name\": \"ypos\",\"value\": \"" + yaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }
          
          values = this.repositionEntity(values);
          values[7] = this.server.toString();
          values = this.removeTheElement(values, 10);
          params = this.removeTheElement(params, 10);
        }
        
        try {
          if (classname.toString().equals("Device")) {
            Method m = this.getClass().getMethod(method.getString("method") + classname.toString(), params);
            m.invoke(this, values);
          }
          else {
            Method m = this.simulation.getClass().getMethod(method.getString("method") + classname.toString(), params);
            m.invoke(this.simulation, values);
          }
        }
        catch(Exception e) {
          println("exception: " + e.getCause());
        }
      }
    }

    device.json = null;
  }
  
  boolean entityIsTransfering(Entity entity) {
    if (entity.current_device.equals(this.server.toString()) && entity.isAlive() && (entity.xpos > width - entity.xsize / 2 || entity.xpos < entity.xsize / 2 || entity.ypos > height - entity.ysize / 2 || entity.ypos < entity.ysize / 2)) {
      int index = 0;
      
      JSONAction message = new JSONAction().setClass("Entity").addMethod("update")
      .addArg("update", "name", "String", entity.name)
      .addArg("update", "alive", "Boolean", "true")
      .addArg("update", "xspd", "int", Integer.toString(entity.xspd))
      .addArg("update", "yspd", "int", Integer.toString(entity.yspd))
      .addArg("update", "xsize", "int", Integer.toString(entity.xsize))
      .addArg("update", "ysize", "int", Integer.toString(entity.ysize))
      .addArg("update", "direction", "String", entity.direction)
      .addArg("update", "current_device", "String", entity.current_device);
      
      int column_length = this.globals.length;
      int row_length = this.globals[0].length;
      for (int i = 0; i < column_length; i++) {
        for (int j = 0; j < row_length; j++) {
          if (this.globals[i][j] != null && this.globals[i][j].equals(this.server.toString())) {
              
            if (entity.ypos < entity.ysize / 2 && entity.yspd < 0) {
              if (this.globals[i-1][j] != null) {
                index = this.indexOfClient(this.globals[i-1][j]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int xaxis = this.positionToNewScreen(entity.xpos, 'x', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(xaxis))
                .addArg("update", "ypos", "int", Integer.toString(entity.ypos))
                .addArg("update", "device_direction", "String", "up");

                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.ypos > height - entity.ysize / 2 && entity.yspd > 0) {
              if (this.globals[i+1][j] != null) {
                index = this.indexOfClient(this.globals[i+1][j]);
              }
              
              if (index >= 0 && devices[index] != null ) {
                String client_name = this.clientResolutions[index][0];
                int xaxis = this.positionToNewScreen(entity.xpos, 'x', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(xaxis))
                .addArg("update", "ypos", "int", Integer.toString(entity.ypos - height))
                .addArg("update", "device_direction", "String", "down");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.xpos < entity.xsize / 2 && entity.xspd < 0) {
              if (this.globals[i][j-1] != null) {
                index = this.indexOfClient(this.globals[i][j-1]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int yaxis = this.positionToNewScreen(entity.ypos, 'y', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(entity.xpos))
                .addArg("update", "ypos", "int", Integer.toString(yaxis))
                .addArg("update", "device_direction", "String", "left");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.xpos > width - entity.xsize / 2 && entity.xspd > 0) {
              if (this.globals[i][j+1] != null) {
                index = this.indexOfClient(this.globals[i][j+1]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int yaxis = this.positionToNewScreen(entity.ypos, 'y', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(entity.xpos - width))
                .addArg("update", "ypos", "int", Integer.toString(yaxis))
                .addArg("update", "device_direction", "String", "right");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
          }
        }
      }

    }
    return false;
  }
  
  boolean informNeighbours() {
    if (this.simulation.cells == null || this.simulation.cells[0][0] == null) {
      return false;
    }

    boolean sent = false;
    
    int x_length = this.simulation.cells.length,
        y_length = this.simulation.cells[0].length;
        
    String[][] top = new String[2][x_length], 
              bottom = new String[2][x_length],
              left = new String[2][y_length], 
              right = new String[2][y_length];

    String[] stringColumns = new String[x_length];

    for (int x = 0; x < x_length; x++) {
      stringColumns[x] = "";
      for (int y = 0; y < y_length; y++) {
        if (this.simulation.cells[x][y] == null) continue;

        stringColumns[x] += Boolean.toString(this.simulation.cells[x][y].alive);
        if (y < y_length-1) stringColumns[x] += ",";

      }

    }
    
    int index = 0;
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    String mergedArray = String.join("|", stringColumns);
    
    JSONAction message = new JSONAction();
    for (int i = 0; i < column_length; i++) {
      for (int j = 0; j < row_length; j++) {
        if (this.globals[i][j] != null && this.globals[i][j].equals(this.server.toString())) {
          if (i > 0 && this.globals[i-1][j] != null) {
            index = this.indexOfClient(this.globals[i-1][j]);
            
            if (index >= 0 && devices[index] != null) {
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "bottom")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (i < column_length && this.globals[i+1][j] != null) {
            index = this.indexOfClient(this.globals[i+1][j]);
            
            if (index >= 0 && devices[index] != null) {
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "top")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (j > 0 && this.globals[i][j-1] != null) {
            index = this.indexOfClient(this.globals[i][j-1]);
            
            if (index >= 0 && devices[index] != null) {
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "right")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (j < row_length && this.globals[i][j+1] != null) {
            index = this.indexOfClient(this.globals[i][j+1]);
            
            if (index >= 0 && devices[index] != null) {
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "left")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
        }
      }
    }
    
    return sent;
  }
  
  
  String[] arrayBooleanToString(boolean[] booleanArray) {
    int arrayLength = booleanArray.length;
    String[] stringArray = new String[arrayLength];
    
    for(int i = 0; i < arrayLength; i++)
      stringArray[i] = Boolean.toString(booleanArray[i]);
    
    return stringArray;
  }
  
  boolean[] arrayStringTBoolean(String[] stringArray) {
    int arrayLength = stringArray.length;
    boolean[] booleanArray = new boolean[arrayLength];
    
    for(int i = 0; i < arrayLength; i++)
      booleanArray[i] = Boolean.parseBoolean(stringArray[i]);
    
    return booleanArray;
  }
  
  String nextClient(Client client, String direction) {
    for (int i = 0; i < this.globals.length; i++) {
      for (int j = 0; j < this.globals[i].length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.toString())) {
            final String regex = "[\\d]+.[\\d]+.[\\d]+.[\\d]+";
            switch(direction) {
              case "up":
                if (i > 0 && this.globals[i-1][j] != null && ! this.regexMatch(regex, this.globals[i-1][j]))
                  return this.globals[i-1][j];
              case "down":
                if (i < this.globals.length && this.globals[i+1][j] != null && ! this.regexMatch(regex, this.globals[i+1][j]))
                  return this.globals[i+1][j];
              case "left":
                if (j > 0 && this.globals[i][j-1] != null && ! this.regexMatch(regex, this.globals[i][j-1]))
                  return this.globals[i][j-1];
              case "right":
                if (j < this.globals[i].length && this.globals[i][j+1] != null && ! this.regexMatch(regex, this.globals[i][j+1]))
                  return this.globals[i][j+1];
              default:
                return this.globals[i][j];
            }
          }
        }
      }
    }
    return "";
  }
  
  Object[] repositionEntity(Object[] values) {
    Entity[] entity = this.simulation.entity;
    for (int i = 0; i < entity.length; i++) {
      
      //if entity name equals value name
      if (entity[i] != null && entity[i].name.toString().equals(values[0].toString())) {
        
        if (values[10].toString().equals("left")) {
          values[8] = Integer.valueOf(values[8].toString()) + width;
          values[9] = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("right")) {
          values[8] = Integer.valueOf(values[8].toString()) - width;
          values[9] = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("up")) {
          values[9] = Integer.valueOf(values[9].toString()) + height;
          values[8] = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("down")) {
          values[9] = Integer.valueOf(values[9].toString()) - height;
          values[8] = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), "server");
        }
      }
    }
    
    return values;
  }
  
  void setResolutionDevice(String name, int device_width, int device_height) {
    for (int i = 0; i < this.clientResolutions.length; i++) {
      if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(name)) {
        this.clientResolutions[i] = new String[] {
          name, 
          String.valueOf(device_width), 
          String.valueOf(device_height),
        };
        return;
      }
    }
  }
  
   String[] getResolutionDevice(String name) {
    for (int i = 0; i < this.clientResolutions.length; i++) {
      if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(name)) {
        return this.clientResolutions[i];
      }
    }
    
    return new String[] {"server", String.valueOf(width), String.valueOf(height)};
  }
  
  int positionToNewScreen(int pos, char axis, String from_client, String to_client) {
    String[] resolution = getResolutionDevice(from_client);
    String[] new_resolution = getResolutionDevice(to_client);
    float newpos = 0;
    
    if (axis == 'x') {
      newpos = ((float)pos / parseInt(resolution[1])) * 100;
      newpos = newpos * parseInt(new_resolution[1]) / 100;
    }
    else if (axis == 'y') {
      newpos = ((float)pos / parseInt(resolution[2])) * 100;
      newpos = newpos * parseInt(new_resolution[2]) / 100;
    }
    
    return (int) newpos;
  }
  
  boolean regexMatch(String regex, String content) {
    final Pattern pattern = Pattern.compile(regex);
    final Matcher matcher = pattern.matcher(content);
    if (matcher.find()) {
      return true;
    }
    return true;
  }
  
  int indexOfClient(String name) {
    for (int i = 0; i < this.devices.length; i++) {
      if (this.devices[i] != null && this.devices[i].toString().equals(name))
        return i;
    }
    return -1;
  }
  
  Object[] removeTheElement(Object[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      } 

      // Create another array of size one less 
      Object[] anotherArray = new Object[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  Class[] removeTheElement(Class[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      } 

      // Create another array of size one less 
      Class[] anotherArray = new Class[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  void debug(String type) {
    switch(type) {
      case "globals":
        for (int i = 0; i < this.globals.length; i++) {
          println("Row " + i + ":");
          for (int j = 0; j < this.globals[i].length; j++) {
            print("[" + j + "] => " + this.globals[i][j] + " ");
          }
          println();
        }
        break;
      case "entity":
        println(this.simulation.entity[0].xpos, this.simulation.entity[0].ypos);
        break;
      case "resolutions": case "res":
        for (int i = 0; i < this.clientResolutions.length; i++) {
          println("[" + this.clientResolutions[i][0] + "] => " + this.clientResolutions[i][1] + "x" + this.clientResolutions[i][2]);
        }
        break;
      case "clients" : default:
        for (int i = 0; i < this.devices.length; i++) {
          println("[" + i + "] => " + this.devices[i]);
        }
    }
  }
  
}
