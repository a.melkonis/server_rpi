import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.net.*; 
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 
import java.util.Set; 
import java.lang.reflect.Method; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class server_rpi extends PApplet {







Device device;
DeviceWorld area;
Client client;

XML config;
int[] screen = new int[2];
int max_entities = 0;

public void settings() {
  config = loadXML("config.xml");
  
  device = new Device(this, PApplet.parseInt(config.getChild("screen_width").getContent()), PApplet.parseInt(config.getChild("screen_height").getContent()));
  device.initServer(PApplet.parseInt(config.getChild("port").getContent()));
  
  max_entities = PApplet.parseInt(config.getChild("max_entities").getContent());
  area = new DeviceWorld(device.server);
  size(area.screen[0], area.screen[1]);
}

public void setup(){
  if (area.position[0] >= 0 && area.position[1] >= 0) {
    surface.setLocation(area.position[0], area.position[1]);
  }
  
  if (config.getChild("simulation") != null) {
    /* Setup Simulation */
    if (config.getChild("simulation").getContent().equals("ball")) {
      // Ball
      for (int i = 0; i < max_entities; i++) {
        area.setEntity(new Ball((i * i) + 35, (i * i) + 35, device.server.toString()), i);
        area.entity[i].setName("ball_" + i);
      }

      // Bounce Ball
      area.setSimulation(new Bounce(max_entities, area.entity));
    }
    else if (config.getChild("simulation").getContent().equals("cell")) {
      // Cell
      int max_cell_entities = (width/max_entities * height/max_entities);
      //max_cell_entities *= max_cell_entities; 
      area.entity = new Entity[max_cell_entities];
      for (int i = 0; i < max_cell_entities; i++) {
        area.setEntity(new Cell((i * i) + 35, (i * i) + 35, 10/*max_entities*/, device.server.toString()), i);
        area.entity[i].setName("cell_" + i);
      }

      // Life Cell
      area.setSimulation(new Life(max_cell_entities, area.entity));
    } 
  }
}

public void draw(){
  area.doClientAction(device);
  area.simulation.update();
  area.simulation.draw();
  
  //area.debug("globals");
  //area.debug("res");
  if (! isEmpty(area.devices)) {
    area.informNeighbours();
    for(int i = 0; i < area.simulation.entity.length; i++) {
      if (area.simulation.entity[i] != null && area.entityIsTransfering(area.simulation.entity[i])) {
        //area.simulation.entity[i].alive = false;
      }
    }
  }
  
  client = area.server.available();
  if (client != null) {
    JSONObject json;
    String data;
    data = client.readString();
    println("From client" + data);
    if (data != null) {
      try {
        device.json = parseJSONObject(data);   
        device.last_client = client.toString();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
}

public void serverEvent(Server server, Client client) {
  area.updateClients(client);
}

public void disconnectEvent(Client client) {
  area.removeInactive(client);
}

public boolean isEmpty(Object[] array) {
  for(Object item: array) {
    if (item != null)
      return false;
  }
  return true;
}

public void keyPressed() {
  area.simulation.keyPressed();
}

public void mousePressed() {
  area.simulation.mousePressed();
}

public void mouseDragged() {
  area.simulation.mouseDragged();
}
class Ball extends Entity {
  XML xml;
  float gravity;
  float bounce;
  
  Ball(int x,int y, String current_device) {
    this.name = "Ball";
    this.current_device = current_device;
    xml = loadXML("config_ball.xml");
    this.xsize = PApplet.parseInt(xml.getChild("ball_size").getContent());
    this.ysize = xsize;
    this.xradius = xsize / 2;
    this.yradius = ysize / 2;
    
    this.xpos = x;
    this.ypos = y;
    this.xspd = PApplet.parseInt(xml.getChild("speed").getContent());
    this.yspd = PApplet.parseInt(xml.getChild("speed").getContent());
    //xdir = 1;
    //ydir = 1;
    
    this.gravity = 0.98f;
    this.bounce = -1;
  }
  
  public void bounceOld() {
    if (xpos > width - xsize / 2 || xpos < xsize / 2) {
      xspd *= -1;
      //if (xspd > 0) {
      //  xspd -= bounce;
      //}
    }
    if (ypos > height - ysize / 2 || ypos < ysize / 2) {
      yspd *= -1;
      //if (yspd > 0) {
      //  yspd *= bounce;
      //}
    }
    
    //println(yspd);
  }
  
  public void update() {
    //yspd += gravity;
    //ypos += yspd;
    //if (this.current_device.equals(server_rpi.this.toString())) {
      move(direction);
    //}
  }
  
  public void draw() {
    if (this.isAlive()) {
      fill(0);
      ellipse(xpos, ypos, xsize, ysize);
    }
  }

}
class Bounce extends Simulation {
  
  Bounce(int max_entities, Entity[] entities) {
    super(max_entities);
    for (int i = 0; i < entities.length; i++) {
      this.setEntity(entities[i], i);
      this.entity[i].direction = "all";
      this.entity[i].alive = true;
    }
    
    this.setupWalls();
  }
  
  public void draw() {
    background(255);
    for (int i = 0; i < this.wall.length; i++) {
      if (this.wall[i] != null && this.wall[i].isAlive())
        this.wall[i].draw();
    }
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].draw();
      }
    }
  }
  
  public void update() {
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].update();
        for (int j = 0; j < this.wall.length; j++) {
          if (this.wall[j] != null) {
            this.entity[i].bounce(this.wall[j]);
            this.entity[i].canMove(this.wall[j]);
          }
        }
        
        if (i < this.entity.length - 1 && this.entity[i+1] != null)
          this.entity[i].bounce(this.entity[i+1]);
        
        
        this.entity[i].screenOut();
      }
    }
  }

  public void keyPressed() {
    for (int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        if (keyCode == UP) {
          this.entity[i].direction = "up";
          if (this.entity[i].yspd > 0)
            this.entity[i].yspd *= -1;
        } else if (keyCode == DOWN) {
          this.entity[i].direction = "down";
          if (this.entity[i].yspd < 0)
            this.entity[i].yspd *= -1;
        } else if (keyCode == LEFT) {
          this.entity[i].direction = "left";
          if (this.entity[i].xspd > 0)
            this.entity[i].xspd *= -1;
        } else if (keyCode == RIGHT) {
          this.entity[i].direction = "right";
          if (this.entity[i].xspd < 0)
            this.entity[i].xspd *= -1;
        }
        
        this.entity[i].update();
      }
    }
  }

  public void mousePressed() {
    if (mouseButton == RIGHT) {
      for (int i = 0; i < this.entity.length; i++) {
        if (this.entity[i] != null) {
            this.entity[i].direction = "all";
            this.entity[i].alive = true;
            this.entity[i].xpos = 50;
            this.entity[i].ypos = 50;
            if (device != null) {
              this.entity[i].current_device = device.server.toString();
            }
          
          this.entity[i].update();
        }
      }
    }
  }

  public void mouseDragged() {
    boolean can_move;
    for (int i = 0; i < this.entity.length; i++) {
      can_move = true;
      if (this.entity[i] != null && this.entity[i].isAlive()) {
        this.entity[i].update();
        for (int j = 0; j < this.wall.length; j++) {
          if (this.wall[j] != null) {
            if (!this.entity[i].canMove(this.wall[j])) {
              can_move = false;
            }
          }
        }
        
        if (i < this.entity.length -1 && this.entity[i+1] != null) {
          if (!this.entity[i].canMove(this.entity[i+1])) {
            can_move = false;
          }
        }
        
        if (can_move)
          this.entity[i].mouseDragged();
      }
    }
  }
  
}
class Cell extends Entity {
  XML xml;
  float gravity;
  float bounce;
  
  Cell(int x,int y, int size, String current_device) {
    this.name = "Cell";
    this.current_device = current_device;
    xml = loadXML("config_cell.xml");
    if (size <= 0) {
      this.xsize = PApplet.parseInt(xml.getChild("cell_size").getContent());
    } else {
      this.xsize = size;
    }
    this.ysize = xsize;
    
    this.xpos = x;
    this.ypos = y;
  }
  
  public void update() {
    //if (this.current_device.equals(server_rpi.this.toString())) {
      //move(direction);
    //}
  }
  
  public void draw() {
    if (this.isAlive()) {
      fill(0);
      rect(xpos, ypos, xsize, ysize);
    }
  }

}
class DeviceWorld extends Area {
  Server server;
  Client[] devices;
  int[][] activeGlobal;
  XML config;
  String[][] globals;
  String[][] clientResolutions;
  
  DeviceWorld(Server server) {
    this.server = server;
    config = loadXML("config.xml");
    this.devices = new Client[PApplet.parseInt(config.getChild("max_clients").getContent())];
    this.constructAreas();
    this.fillAreas();
  }
  
  /* Create globals array */
  public void constructAreas() {
    //Set globals array from the xml
    XML[] rows = config.getChild("globals").getChildren("row");
    int x = 0;
    int y = rows.length;
    
    for(int i = 0; i < y; i++) {
      int areas = rows[i].getChildren("area").length;
      if (areas > x) {
        x = areas;
      }
    }
    
    globals = new String[y][x];
    clientResolutions = new String[config.getChild("max_clients").getIntContent()][3];
  }
  
  /* Fill all rows from the globals array with client IPs */
  public void fillAreas() {
    XML[] rows = config.getChild("globals").getChildren("row");
    XML[] areas;
    boolean serverPlaced = false;
    
    for(int i = 0; i < rows.length; i++) {
      areas = rows[i].getChildren("area");
      
      for(int j = 0; j < areas.length; j++) {
        this.globals[i][j] = areas[j].getContent();
        
        if ( ! serverPlaced && areas[j].getContent().equals("127.0.0.1")) {
          this.activeGlobal = new int[][] {{i}, {j}};
          this.globals[i][j] = this.server.toString();
          serverPlaced = true;
        }
      }
    }
  }
  
  /* Add new connected client to the clients list */
  public void updateClients(Client client) {
    // Get next open position to add client in globals
    for (int i = 0; i < this.devices.length; i++) {    
      if (this.devices[i] == null) {
        this.devices[i] = client;
        this.clientResolutions[i] = new String[] {client.toString(), "set", "set"};
        break;
      }
    }
    
    int[] location = {-1, -1};
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    int index = 0;
    for (int i = 0; i < column_length; i++) {
      for (int j = 0; j < row_length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.ip()) && location[0] == -1) {
            location[0] = i;
            location[1] = j;
          }
        }
        
        if (client.toString() == this.globals[i][j]) {
          return;
        }
      }
    }
    
    if (location[0] != -1) {
      this.globals[location[0]][location[1]] = client.toString();
      
      int i = location[0];
      int j = location[1];
      
      //If walls are set
      if (this.simulation.wall != null) {
        JSONAction message = new JSONAction().setClass("Wall").addMethod("delete");
        JSONAction message_new_client = new JSONAction().setClass("Wall").addMethod("delete");
        
        //Delete walls
        if (this.simulation.wall.length > 0) {
          for (int a = j; a > 0; a--) {
            if (a-1 >= 0) {
              if (this.globals[i][a-1] != null && this.globals[i][a-1].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("left");
                else {
                  message_new_client.addArg("delete", "name", "String", "left");
                  //this.devices[index].write(message_new_client.getJSON());
                }
                
                index = this.indexOfClient(this.globals[i][a-1]);
                if (index == -1)
                  this.simulation.deleteWall("right");
                else {
                  message.addArg("delete", "name", "String", "right");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = j; a < row_length-1; a++) {
            if (a+1 <= row_length) {
              if (this.globals[i][a+1] != null && this.globals[i][a+1].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("right");
                else {
                  message_new_client.addArg("delete", "name", "String", "right");
                  //this.devices[index].write(message_new_client.getJSON());
                }
                
                index = this.indexOfClient(this.globals[i][a+1]);
                if (index == -1)
                  this.simulation.deleteWall("left");
                else {
                  message.addArg("delete", "name", "String", "left");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = i; a > 0; a--) {
            if (a-1 >= 0) {
              if (this.globals[a-1][j] != null && this.globals[a-1][j].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("up");
                else {
                  message_new_client.addArg("delete", "name", "String", "up");
                  //this.devices[index].write(message_new_client.getJSON());
                }
                
                index = this.indexOfClient(this.globals[a-1][j]);
                if (index == -1)
                  this.simulation.deleteWall("down");
                else {
                  message.addArg("delete", "name", "String", "down");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }
          
          message.reset();
          message.setClass("Wall").addMethod("delete");
          
          for (int a = i; a < column_length-1; a++) {
            if (a+1 <= column_length) {
              if (this.globals[a+1][j] != null && this.globals[a+1][j].contains("@")) {
                index = this.indexOfClient(client.toString());
                if (index == -1)
                  this.simulation.deleteWall("down");
                else {
                  message_new_client.addArg("delete", "name", "String", "down");
                  //this.devices[index].write(message_new_client.getJSON());
                }
                
                index = this.indexOfClient(this.globals[a+1][j]);
                if (index == -1)
                  this.simulation.deleteWall("up");
                else {
                  message.addArg("delete", "name", "String", "up");
                  this.devices[index].write(message.getJSON());
                }
              }
              break;
            }
          }

          if (message_new_client.hasArgs("delete")) {
            index = this.indexOfClient(client.toString());
            this.devices[index].write(message_new_client.getJSON());
          }
        }
      }
    }
  }
  
  public void removeInactive(Client client) {
    for (int i = 0; i < this.devices.length; i++) {
      if (client == this.devices[i]) {
        this.devices[i] = null;
        this.clientResolutions[i] = new String[3];
      }
    }
    
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    int index = 0;
    JSONAction message = new JSONAction();
    for (int i = 0; i < this.globals.length; i++) {
      for (int j = 0; j < this.globals[i].length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.toString())) {
            this.globals[i][j] = client.ip();
            
            //Create walls
            if (this.simulation.wall != null && this.simulation.wall.length > 0) {
              for (int a = j; a > 0; a--) {
                if (a-1 >= 0) {
                  if (this.globals[i][a-1] != null && this.globals[i][a-1].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "right");
                    index = this.indexOfClient(this.globals[i][a-1]);
                    if (index == -1)
                      this.simulation.createWall("right");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = j; a < row_length-1; a++) {
                if (a+1 <= row_length) {
                  if (this.globals[i][a+1] != null && this.globals[i][a+1].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "left");
                    index = this.indexOfClient(this.globals[i][a+1]);
                    if (index == -1)
                      this.simulation.createWall("left");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = i; a > 0; a--) {
                if (a-1 >= 0) {
                  if (this.globals[a-1][j] != null && this.globals[a-1][j].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "down");
                    index = this.indexOfClient(this.globals[a-1][j]);
                    if (index == -1)
                      this.simulation.createWall("down");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
              
              message.reset();
              
              for (int a = i; a < column_length-1; a++) {
                if (a+1 <= column_length) {
                  if (this.globals[a+1][j] != null && this.globals[a+1][j].contains("@")) {
                    message.setClass("Wall").addMethod("create").addArg("create", "name", "String", "up");
                    index = this.indexOfClient(this.globals[a+1][j]);
                    if (index == -1)
                      this.simulation.createWall("up");
                    else
                      this.devices[index].write(message.getJSON());
                    break;
                  }
                }
              }
            }
            
            return;
          }
        }
      }
    }
  }
  
  public void doClientAction(Device device) {
    if (device.json == null)
      return;
    
    Set Classes = device.json.keys();
    for(Object classname : Classes) {
      JSONObject methods = device.json.getJSONObject(classname.toString()).getJSONObject("methods");
      Set method_indexes = methods.keys();
      
      for (Object method_index : method_indexes) {
        JSONObject method = methods.getJSONObject(method_index.toString());
        JSONObject args = method.getJSONObject("args");
        Class[] params = new Class[args.size()];
        Object[] values = new Object[args.size()];
        
        for (int i = 0; i < args.size(); i++) {
          JSONObject argument = args.getJSONObject(String.valueOf(i));
          
          switch(argument.getString("type")) {
            case "String":
              params[i] = String.class;
              values[i] = argument.getString("value");
              break;
            case "int": case "Integer":
              params[i] = int.class;
              values[i] = argument.getInt("value");
              break;
            case "float": case "Float":
              params[i] = float.class;
              values[i] = argument.getFloat("value");
              break;
            case "double": case "Double":
              params[i] = double.class;
              values[i] = argument.getDouble("value");
              break;
            case "boolean": case "Boolean":
              params[i] = boolean.class;
              values[i] = argument.getBoolean("value");
              break;
            case "char": case "Character":
              params[i] = char.class;
              values[i] = argument.getString("value");
              break;
          }
        }
        
        if (method.getString("method").equals("setResolution")) {
          for (int i = 0; i < this.clientResolutions.length; i++) {
            if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(device.last_client)) {
              this.clientResolutions[i][0] = values[0].toString();
            }
          }
        }
        
        if (method.getString("method").equals("check")) {
          int column_length = this.globals.length;
          int row_length = this.globals[0].length;
          int i = 0, j = 0;
          boolean found = false;
          
          try {
            for (i = 0; i < column_length; i++) {
              for (j = 0; j < row_length; j++) {
                if (this.globals[i][j] != null && this.globals[i][j].equals(device.last_client)) {
                  found = true;
                  break;
                } 
              }
              if (found == true)
                break;
            }


            if (values[0].equals("bottom") && i-1 >= 0 && !this.globals[i-1][j].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i-1][j]);
              println("top " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("top") && i+1 < this.globals.length && !this.globals[i+1][j].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i+1][j]);
              println("bot " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("right") && j-1 >= 0 && !this.globals[i][j-1].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i][j-1]);
              println("left " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else if (values[0].equals("left") && j+1 < this.globals[0].length && !this.globals[i][j+1].equals("127.0.0.1")) {
              int index = this.indexOfClient(this.globals[i][j+1]);
              println("right " + index);
              
              if (index >= 0) {
                this.devices[index].write(device.json.toString());
                device.json = null;
                return;
              }
            }
            else {
              //println(values[0] + " not server or online client");
              device.json = null;
              return;
            }
          }
          catch (Exception e) {
            println("exception: " + e.getCause());
          }
        }
        
        if (method.getString("method").equals("update")) {
          int column_length = this.globals.length;
          int row_length = this.globals[0].length;
          int i = 0, j = 0;
          boolean found = false;
          
          for (i = 0; i < column_length; i++) {
            for (j = 0; j < row_length; j++) {
              if (this.globals[i][j] != null && this.globals[i][j].equals(device.last_client)) {
                found = true;
                break;
              } 
            }
            if (found == true)
              break;
          }
          
          if (values[10].toString().equals("up") && i > 0 && this.globals[i-1][j] != null) {
            int index = this.indexOfClient(this.globals[i-1][j]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int xaxis = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), client_name);
              String string_json = device.json.toString().replace(
                 "\"name\": \"xpos\",\"value\": \"" + values[8].toString() + "\",",
                 "\"name\": \"xpos\",\"value\": \"" + xaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }  
          else if (values[10].toString().equals("down") && i < column_length && this.globals[i+1][j] != null) {
            int index = this.indexOfClient(this.globals[i+1][j]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int xaxis = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"xpos\",\"value\": \"" + values[8].toString() + "\",",
                 "\"name\": \"xpos\",\"value\": \"" + xaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }  
          else if (values[10].toString().equals("left") && j > 0 && this.globals[i][j-1] != null) {
            int index = this.indexOfClient(this.globals[i][j-1]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int yaxis = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"ypos\",\"value\": \"" + values[9].toString() + "\",",
                 "\"name\": \"ypos\",\"value\": \"" + yaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }
          else if (values[10].toString().equals("right") && j < row_length && this.globals[i][j+1] != null) {
            int index = this.indexOfClient(this.globals[i][j+1]);
            
            if (index >= 0) {
              String client_name = this.clientResolutions[index][0];
              int yaxis = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), client_name); 
              String string_json = device.json.toString().replace(
                 "\"name\": \"ypos\",\"value\": \"" + values[9].toString() + "\",",
                 "\"name\": \"ypos\",\"value\": \"" + yaxis + "\",");
                 
              this.devices[index].write(string_json);
              device.json = null;
              return;
            }
          }
          
          values = this.repositionEntity(values);
          values[7] = this.server.toString();
          values = this.removeTheElement(values, 10);
          params = this.removeTheElement(params, 10);
        } //<>//
        
        try {
          if (classname.toString().equals("Device")) {
            Method m = this.getClass().getMethod(method.getString("method") + classname.toString(), params);
            m.invoke(this, values);
          }
          else {
            Method m = this.simulation.getClass().getMethod(method.getString("method") + classname.toString(), params);
            m.invoke(this.simulation, values);
          }
        }
        catch(Exception e) {
          println("exception: " + e.getCause());
        }
      }
    }

    device.json = null;
  }
  
  public boolean entityIsTransfering(Entity entity) {
    if (entity.current_device.equals(this.server.toString()) && entity.isAlive() && (entity.xpos > width - entity.xsize / 2 || entity.xpos < entity.xsize / 2 || entity.ypos > height - entity.ysize / 2 || entity.ypos < entity.ysize / 2)) { //<>//
      int index = 0;
      //String message = entity.xpos + "," + entity.ypos + "," + "," + entity.xspd + "," + entity.yspd + "," + entity.xsize + "," + entity.ysize;
      
      JSONAction message = new JSONAction().setClass("Entity").addMethod("update")
      .addArg("update", "name", "String", entity.name)
      .addArg("update", "alive", "Boolean", "true")
      .addArg("update", "xspd", "int", Integer.toString(entity.xspd))
      .addArg("update", "yspd", "int", Integer.toString(entity.yspd))
      .addArg("update", "xsize", "int", Integer.toString(entity.xsize))
      .addArg("update", "ysize", "int", Integer.toString(entity.ysize))
      .addArg("update", "direction", "String", entity.direction)
      .addArg("update", "current_device", "String", entity.current_device);
      
      int column_length = this.globals.length;
      int row_length = this.globals[0].length;
      for (int i = 0; i < column_length; i++) {
        for (int j = 0; j < row_length; j++) {
          if (this.globals[i][j] != null && this.globals[i][j].equals(this.server.toString())) {
              
            if (entity.ypos < entity.ysize / 2 && entity.yspd < 0) {
              if (this.globals[i-1][j] != null) {
                index = this.indexOfClient(this.globals[i-1][j]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int xaxis = this.positionToNewScreen(entity.xpos, 'x', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(xaxis))
                .addArg("update", "ypos", "int", Integer.toString(entity.ypos))
                .addArg("update", "device_direction", "String", "up");

                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.ypos > height - entity.ysize / 2 && entity.yspd > 0) {
              if (this.globals[i+1][j] != null) {
                index = this.indexOfClient(this.globals[i+1][j]);
              }
              
              if (index >= 0 && devices[index] != null ) {
                String client_name = this.clientResolutions[index][0];
                int xaxis = this.positionToNewScreen(entity.xpos, 'x', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(xaxis))
                .addArg("update", "ypos", "int", Integer.toString(entity.ypos - height))
                .addArg("update", "device_direction", "String", "down");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.xpos < entity.xsize / 2 && entity.xspd < 0) {
              if (this.globals[i][j-1] != null) {
                index = this.indexOfClient(this.globals[i][j-1]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int yaxis = this.positionToNewScreen(entity.ypos, 'y', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(entity.xpos))
                .addArg("update", "ypos", "int", Integer.toString(yaxis))
                .addArg("update", "device_direction", "String", "left");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
            
            else if (entity.xpos > width - entity.xsize / 2 && entity.xspd > 0) {
              if (this.globals[i][j+1] != null) {
                index = this.indexOfClient(this.globals[i][j+1]);
              }
              
              if (index >= 0 && devices[index] != null) {
                String client_name = this.clientResolutions[index][0];
                int yaxis = this.positionToNewScreen(entity.ypos, 'y', "server", client_name);
                
                message.addArg("update", "xpos", "int", Integer.toString(entity.xpos - width))
                .addArg("update", "ypos", "int", Integer.toString(yaxis))
                .addArg("update", "device_direction", "String", "right");
                
                this.devices[index].write(message.getJSON());
                return true;
              }
            }
          }
        }
      }

    }
    return false;
  }
  
  public boolean informNeighbours() {
    if (this.simulation.cells == null || this.simulation.cells[0][0] == null) {
      return false;
    }

    boolean sent = false;
    
    int x_length = this.simulation.cells.length,
        y_length = this.simulation.cells[0].length;
        
    String[][] top = new String[2][x_length], 
              bottom = new String[2][x_length],
              left = new String[2][y_length], 
              right = new String[2][y_length];

    String[] stringColumns = new String[x_length];

    for (int x = 0; x < x_length; x++) {
      stringColumns[x] = "";
      for (int y = 0; y < y_length; y++) {
        if (this.simulation.cells[x][y] == null) continue;

        // if ( (y > 1 && y < y_length-2) && (x > 1 && x < x_length-2) ) {
        //   //continue;
        // }

        stringColumns[x] += Boolean.toString(this.simulation.cells[x][y].alive);
        if (y < y_length-1) stringColumns[x] += ",";
        
        // if (y == 0 && x < x_length) {
        //   top[0][x] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        // else if (y == 1 && x < x_length) {
        //   top[1][x] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        
        // if (y == y_length-2 && x < x_length) {
        //   bottom[0][x] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        // else if (y == y_length-1 && x < x_length) {
        //   bottom[1][x] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        
        
        // if (x == 0 && y < y_length) {
        //   left[0][y] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        // else if (x == 1 && y < y_length) {
        //   left[1][y] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        
        // if (x == x_length-2 && y < y_length) {
        //   right[0][y] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
        // else if (x == x_length-1 && y < y_length) {
        //   right[1][y] = Boolean.toString(this.simulation.cells[x][y].alive);
        // }
      }

    }
    
    int index = 0;
    int column_length = this.globals.length;
    int row_length = this.globals[0].length;
    //String mergedArray;
    String mergedArray = String.join("|", stringColumns);
    
    JSONAction message = new JSONAction();
    for (int i = 0; i < column_length; i++) {
      for (int j = 0; j < row_length; j++) {
        if (this.globals[i][j] != null && this.globals[i][j].equals(this.server.toString())) {
          if (i > 0 && this.globals[i-1][j] != null) {
            index = this.indexOfClient(this.globals[i-1][j]);
            
            if (index >= 0 && devices[index] != null) {
              //mergedArray = String.join(",", top[0]) + "|" + String.join(",", top[1]);
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "bottom")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (i < column_length && this.globals[i+1][j] != null) {
            index = this.indexOfClient(this.globals[i+1][j]);
            
            if (index >= 0 && devices[index] != null) {
              //mergedArray = String.join(",", bottom[0]) + "|" + String.join(",", bottom[1]);
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "top")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (j > 0 && this.globals[i][j-1] != null) {
            index = this.indexOfClient(this.globals[i][j-1]);
            
            if (index >= 0 && devices[index] != null) {
              //mergedArray = String.join(",", left[0]) + "|" + String.join(",", left[1]);
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "right")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
          
          if (j < row_length && this.globals[i][j+1] != null) {
            index = this.indexOfClient(this.globals[i][j+1]);
            
            if (index >= 0 && devices[index] != null) {
              //mergedArray = String.join(",", right[0]) + "|" + String.join(",", right[1]);
              message.reset();
              message.setClass("Cell").addMethod("check")
                .addArg("check", "direction", "String", "left")
                .addArg("check", "cells", "String", mergedArray)
                .addArg("check", "current_device", "String", this.server.toString());
              this.devices[index].write(message.getJSON());
              sent = true;
            }
          }
        }
      }
    }
    
    return sent;
  }
  
  
  public String[] arrayBooleanToString(boolean[] booleanArray) {
    int arrayLength = booleanArray.length;
    String[] stringArray = new String[arrayLength];
    
    for(int i = 0; i < arrayLength; i++)
      stringArray[i] = Boolean.toString(booleanArray[i]);
    
    return stringArray;
  }
  
  public boolean[] arrayStringTBoolean(String[] stringArray) {
    int arrayLength = stringArray.length;
    boolean[] booleanArray = new boolean[arrayLength];
    
    for(int i = 0; i < arrayLength; i++)
      booleanArray[i] = Boolean.parseBoolean(stringArray[i]);
    
    return booleanArray;
  }
  
  public String nextClient(Client client, String direction) {
    for (int i = 0; i < this.globals.length; i++) {
      for (int j = 0; j < this.globals[i].length; j++) {
        if (this.globals[i][j] != null) {
          if (this.globals[i][j].equals(client.toString())) {
            final String regex = "[\\d]+.[\\d]+.[\\d]+.[\\d]+";
            switch(direction) {
              case "up":
                if (i > 0 && this.globals[i-1][j] != null && ! this.regexMatch(regex, this.globals[i-1][j]))
                  return this.globals[i-1][j];
              case "down":
                if (i < this.globals.length && this.globals[i+1][j] != null && ! this.regexMatch(regex, this.globals[i+1][j]))
                  return this.globals[i+1][j];
              case "left":
                if (j > 0 && this.globals[i][j-1] != null && ! this.regexMatch(regex, this.globals[i][j-1]))
                  return this.globals[i][j-1];
              case "right":
                if (j < this.globals[i].length && this.globals[i][j+1] != null && ! this.regexMatch(regex, this.globals[i][j+1]))
                  return this.globals[i][j+1];
              default:
                return this.globals[i][j];
            }
          }
        }
      }
    }
    return "";
  }
  
  public Object[] repositionEntity(Object[] values) {
    Entity[] entity = this.simulation.entity;
    for (int i = 0; i < entity.length; i++) {
      
      //if entity name equals value name
      if (entity[i] != null && entity[i].name.toString().equals(values[0].toString())) {
        
        if (values[10].toString().equals("left")) {
          values[8] = Integer.valueOf(values[8].toString()) + width;
          values[9] = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("right")) {
          values[8] = Integer.valueOf(values[8].toString()) - width;
          values[9] = this.positionToNewScreen(Integer.valueOf(values[9].toString()), 'y', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("up")) {
          values[9] = Integer.valueOf(values[9].toString()) + height;
          values[8] = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), "server");
        }
        
        else if (values[10].toString().equals("down")) {
          values[9] = Integer.valueOf(values[9].toString()) - height;
          values[8] = this.positionToNewScreen(Integer.valueOf(values[8].toString()), 'x', values[7].toString(), "server");
        }
      }
    }
    
    return values;
  }
  
  public void setResolutionDevice(String name, int device_width, int device_height) {
    for (int i = 0; i < this.clientResolutions.length; i++) {
      if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(name)) {
        this.clientResolutions[i] = new String[] {
          name, 
          String.valueOf(device_width), 
          String.valueOf(device_height),
        };
        return;
      }
    }
  }
  
   public String[] getResolutionDevice(String name) {
    for (int i = 0; i < this.clientResolutions.length; i++) {
      if (this.clientResolutions[i][0] != null && this.clientResolutions[i][0].equals(name)) {
        return this.clientResolutions[i];
      }
    }
    
    return new String[] {"server", String.valueOf(width), String.valueOf(height)};
  }
  
  public int positionToNewScreen(int pos, char axis, String from_client, String to_client) {
    String[] resolution = getResolutionDevice(from_client);
    String[] new_resolution = getResolutionDevice(to_client);
    float newpos = 0;
    
    if (axis == 'x') {
      newpos = ((float)pos / parseInt(resolution[1])) * 100;
      newpos = newpos * parseInt(new_resolution[1]) / 100;
    }
    else if (axis == 'y') {
      newpos = ((float)pos / parseInt(resolution[2])) * 100;
      newpos = newpos * parseInt(new_resolution[2]) / 100;
    }
    
    return (int) newpos;
  }
  
  public boolean regexMatch(String regex, String content) {
    final Pattern pattern = Pattern.compile(regex);
    final Matcher matcher = pattern.matcher(content);
    if (matcher.find()) {
      return true;
    }
    return true;
  }
  
  public int indexOfClient(String name) {
    for (int i = 0; i < this.devices.length; i++) {
      if (this.devices[i] != null && this.devices[i].toString().equals(name))
        return i;
    }
    return -1;
  }
  
  public Object[] removeTheElement(Object[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      } 

      // Create another array of size one less 
      Object[] anotherArray = new Object[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  public Class[] removeTheElement(Class[] arr, int index) { 
      // If the array is empty
      // or the index is not in array range
      // return the original array
      if (arr == null
          || index < 0
          || index >= arr.length) {

          return arr; 
      } 

      // Create another array of size one less 
      Class[] anotherArray = new Class[arr.length - 1]; 

      // Copy the elements except the index 
      // from original array to the other array 
      for (int i = 0, k = 0; i < arr.length; i++) { 

          // if the index is 
          // the removal element index 
          if (i == index) { 
              continue; 
          } 

          // if the index is not 
          // the removal element index 
          anotherArray[k++] = arr[i]; 
      } 

      // return the resultant array 
      return anotherArray; 
  }
  
  public void debug(String type) {
    switch(type) {
      case "globals":
        for (int i = 0; i < this.globals.length; i++) {
          println("Row " + i + ":");
          for (int j = 0; j < this.globals[i].length; j++) {
            print("[" + j + "] => " + this.globals[i][j] + " ");
          }
          println();
        }
        break;
      case "entity":
        println(this.simulation.entity[0].xpos, this.simulation.entity[0].ypos);
        break;
      case "resolutions": case "res":
        for (int i = 0; i < this.clientResolutions.length; i++) {
          println("[" + this.clientResolutions[i][0] + "] => " + this.clientResolutions[i][1] + "x" + this.clientResolutions[i][2]);
        }
        break;
      case "clients" : default:
        for (int i = 0; i < this.devices.length; i++) {
          println("[" + i + "] => " + this.devices[i]);
        }
    }
  }
  
}
/*
Responsible for every entity and simulation on the screen.
*/

class Area {
  Entity[] entity;
  Simulation simulation;
  int[] screen;
  int[] position;
  XML config;
  
  
  Area() {
    screen = new int[2];
    position = new int[2];
    config = loadXML("config.xml");
    screen[0] = PApplet.parseInt(config.getChild("screen_width").getContent());
    screen[1] = PApplet.parseInt(config.getChild("screen_height").getContent());
    position[0] = PApplet.parseInt(config.getChild("screen_position_x").getContent());
    position[1] = PApplet.parseInt(config.getChild("screen_position_y").getContent());
    entity = new Entity[PApplet.parseInt(config.getChild("max_entities").getContent())];
  }
  
  /* Add new entity to the area */
  public void setEntity(Entity entity, int index) {
      this.entity[index] = entity;
  }
  
  /* Get entity from the area */
  public Entity getEntity(int index) {
      return this.entity[index];
  }
  
  /* Add simulation to the area */
  public void setSimulation(Simulation simulation){
    this.simulation = simulation;
  }
  
}
/*
Responsible for managing device and connections between devices.
*/

class Device {
  int screen[];
  PApplet p;
  String ip_address;
  int port;
  Server server;
  Client client;
  JSONObject json;
  String last_client;
  boolean started;
  
  
  Device(PApplet p, int width, int height) {
    screen = new int[] {width, height};
    this.p = p;
  }
  
  /* Start Server host */
  public void initServer(int port) {
    try {
      this.server = new Server(p, port);
    }
    catch(Exception e) {
        println(e.getCause());
    }
  }
  
  /* Start connection with the Server */
  public Client initClient(String ip_address, int port) {
    this.ip_address = ip_address;
    this.port = port;
    try {
      this.client = new Client(this.p, this.ip_address, this.port);
      
      /* for globars, unfinished */
      JSONAction message = new JSONAction().setClass("Device")
      .addMethod("setResolution")
      .addArg("setResolution", "name", "String", this.client.toString())
      .addArg("setResolution", "device_width", "int", Integer.toString(this.screen[0]))
      .addArg("setResolution", "device_height", "int", Integer.toString(this.screen[1]));
      
      
      this.client.write(message.getJSON());
    }
    catch(Exception e) {
        println(e.getCause());
    }
    finally {
      return this.client;
    }
  }
  
  /* Close server host */
  public void stopServer() {
    this.server.stop();
  }
  
  /* Close connection with the host */
  public void stopClient() {
    this.client.stop();
  }

  /* Close and restart connection withe the host */
  public Client ensureConnection() {
    try {
      if (! this.started && this.client != null) {
        this.client.stop();
        this.client = this.initClient(this.ip_address, this.port);
        this.started = true;
      }
    }
    catch(Exception e) {
        println(e.getCause());
    }
    finally {
      return this.client;
    }
  }
  
}
abstract class Entity {
  int xpos, ypos;
  int xspd, yspd;
  int xsize, ysize, xradius, yradius;
  String name;
  String direction;
  String current_device;
  boolean alive;
  
  float mousex, mousey;
  
  public boolean isAlive(){
    return this.alive;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public void move(String direction) {
    if (direction.contains("left") || direction.contains("right") || direction.contains("all"))
      xpos += xspd;//xdir;
    if (direction.contains("up") || direction.contains("down") || direction.contains("all"))
      ypos += yspd;//ydir;
  }
  
  public void reverse(String direction) {
    if (direction.contains("left") || direction.contains("right") || direction.contains("all"))
      xspd *= -1;
    if (direction.contains("up") || direction.contains("down") || direction.contains("all"))
      yspd *= -1;
  }
  
  public void bounce(Entity entity) {
    if (this.isAlive() && entity.isAlive()) {      
      // X
      if (this.xpos + this.xsize / 2 + this.xspd > entity.xpos - entity.xsize / 2
        && this.xpos - this.xsize / 2 + this.xspd < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 > entity.ypos - entity.ysize / 2
        && this.ypos < entity.ypos + entity.ysize / 2) {
        this.xspd *= -1;
        if (this.direction.contains("right"))
          this.direction.replace("right", "left");
        else if(this.direction.contains("left"))
          this.direction.replace("left", "right");
      }
      
      // Y
      if (this.xpos + this.xsize / 2  > entity.xpos - entity.xsize / 2
        && this.xpos < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 + this.yspd > entity.ypos - entity.ysize / 2
        && this.ypos - this.ysize / 2 + this.yspd < entity.ypos + entity.ysize / 2) {
          this.yspd *= -1;
          if (this.direction.contains("down"))
            this.direction.replace("down", "up");
          else if(this.direction.contains("up"))
            this.direction.replace("up", "down");
      }
    }
  }
  
  public boolean canMove(Entity entity) {
    if (this.xpos + this.xsize / 2 + this.xspd > entity.xpos - entity.xsize / 2
        && this.xpos - this.xsize / 2 + this.xspd < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 > entity.ypos - entity.ysize / 2
        && this.ypos < entity.ypos + entity.ysize / 2) {
          return false;
      }
      
      // Y
      if (this.xpos + this.xsize / 2  > entity.xpos - entity.xsize / 2
        && this.xpos < entity.xpos + entity.xsize / 2
        && this.ypos + this.ysize / 2 + this.yspd > entity.ypos - entity.ysize / 2
        && this.ypos - this.ysize / 2 + this.yspd < entity.ypos + entity.ysize / 2) {
          return false;
      }
    
    return true;
  }
  
  public void bounce(int x, int y) {
    if (xpos > x - xsize || xpos < x + xsize) {
      xspd *= -1;
    }
    if (ypos > y - ysize || ypos < y + ysize) {
      yspd *= -1;
    }
  }
  
  public void bounce() {
    if (xpos > width - xsize / 2 || xpos < xsize / 2) {
      xspd *= -1;
    }
    if (ypos > height - ysize / 2 || ypos < ysize / 2) {
      yspd *= -1;
    }
  }
  
  public void screenOut() {
    if (this.xpos > width + this.xsize || this.xpos < 0 - this.xsize
      || this.ypos > height + this.ysize || this.ypos < 0 - this.ysize) {
        this.alive = false;
      }
  }
  
  public void left() {
    
  }
  
  public void right(){
    
  }
  
  public void angle() {
    
  }
  
  public void update() {
    
  }
  
  public void draw() {
    
  }
  
  public void mouseDragged() {
    if (this.mousex > this.xpos && this.xpos < 0){
      this.xspd *= -1;
    }
    else if (this.mousex < this.xpos && this.xpos > 0) {
      this.xspd *= -1;
    }
    
    if (this.mousey > ypos && this.ypos < 0 )
      this.yspd *= -1;
    else if (this.mousey < this.ypos && this.ypos > 0) {
      this.yspd *= -1;
    }
    
    this.mousex = this.xpos;
    this.mousey = this.ypos;
    this.xpos = mouseX;
    this.ypos = mouseY;
  }

}
/*
Usage: Add target class name as main class target.
Add action methods for the class object to run.
Set arguments for every method attached to the JSON object. Each method must have correct and complete arguments.
*/

class JSONAction {
  private JSONObject json;
  public String classname;
  
  /* Initialize json object  */
  public JSONAction JSONAction() {
    this.json = new JSONObject();
    
    return this;
  }
  
  /* Get JSON as string */
  public String getJSON() {
    return this.json.toString();
  }
  
  /* Get JSON Object */
  public JSONObject getJSONObject() {
    return this.json;
  }
  
  /* Clear JSON Object */
  public void reset() {
    this.json = new JSONObject();
    this.classname = "";
  }
  
  /* Set target classname to run actions */
  public JSONAction setClass(String classname) {
    this.reset();
    this.json.setJSONObject(classname, new JSONObject());
    this.classname = classname;
    
    return this;
  }
  
  /* If target classname has actions to run */
  public boolean hasMethods() {
    return this.json.isNull("methods");
  }
  
  /* If set action methods have arguments included */
  public boolean hasArgs(String method) {
    JSONObject methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    
    for(int i = 0; i < methods.size(); i++) {
      if (methods.getJSONObject(Integer.toString(i)).getString("method").equals(method)) {
        return ! methods.getJSONObject(Integer.toString(i)).isNull("args");
      }
    }
    
    return false;
  }
  
  /* Set action methods for the target classname */
  public JSONAction addMethod(String method) {
    JSONObject methods = new JSONObject();
    if (this.json.getJSONObject(this.classname).isNull("methods")) {
      this.json.getJSONObject(this.classname).setJSONObject("methods", new JSONObject());
    }
    
    methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    int count = 0;
    if (! methods.isNull("0")) {
      count = methods.size();
    }

    
      methods.setJSONObject(Integer.toString(count), new JSONObject()
        .setString("method", method)
    );
    
    return this;
  }
  
  /* Add arguments to the methods */
  public JSONAction addArg(String method_name, String name, String type, String value) {
    JSONObject methods = this.json.getJSONObject(this.classname).getJSONObject("methods");
    JSONObject method = new JSONObject();
    int i;
    for(i = 0; i < methods.size(); i++) {
      if (methods.getJSONObject(Integer.toString(i)).getString("method").equals(method_name)) {
        method = methods.getJSONObject(Integer.toString(i));
        break;
      }
    }
    
    if (method.isNull("args")) {
      method.setJSONObject("args", new JSONObject());
    }
      
    method = method.getJSONObject("args");
    int count = 0;
    if (! method.isNull("0")) {
      count = method.size();
    }
    
    method.setJSONObject(Integer.toString(count), new JSONObject()
      .setString("name", name)
      .setString("type", type)
      .setString("value", value)
    );
    
    return this;
  }
}
abstract class Simulation {
  // array of entities in the simulation
  Entity[] entity;
  // Array of walls for the window edges
  Wall[] wall;
  // Array of cells
  Entity[][] cells;
  // Size of cells
  int cellSize = 5;
  // Array of neighbour cells
  Boolean[][][] neighbourCells;

  Simulation(int max_entities) {
    this.entity = new Entity[max_entities];
    this.cells = new Entity[max_entities][max_entities];
    this.neighbourCells = new Boolean[4][max_entities][max_entities];
  }
  
  /* Set entity to the simulation */
  public void setEntity(Entity entity, int index) {
    this.entity[index] = entity;
  }
  
  public void bounce() {
    
  }
  
  public void explode() {
    
  }
  
  /* Borders arround the window */
  public void setupWalls() {
    this.wall = new Wall[4];
    //top
    this.wall[0] = new Wall(width + 200, 1, width / 2 - 100, 0);
    this.wall[0].setColor(0xff0000ff);
    this.wall[0].name = "up";
    this.wall[0].alive = true;
    //bottom
    this.wall[1] = new Wall(width + 200, 1, width / 2  - 100, height);
    this.wall[1].setColor(0xff0000ff);
    this.wall[1].name = "down";
    this.wall[1].alive = true;
    //left
    this.wall[2] = new Wall(1, height + 200, 0, height / 2 - 100);
    this.wall[2].setColor(0xff0000ff);
    this.wall[2].name = "left";
    this.wall[2].alive = true;
    //right
    this.wall[3] = new Wall(1, height + 200, width, height / 2 - 100);
    this.wall[3].setColor(0xff0000ff);
    this.wall[3].name = "right";
    this.wall[3].alive = true;
    println("walls set");
  }
  
  /* Add wall to a corner */
  public void createWall(String name) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 2 walls to corners (used when connecting or disconnection to host) */
  public void createWall(String name1, String name2) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 3 walls to corners (used when connecting or disconnection to host) */
  public void createWall(String name1, String name2, String name3) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Add 4 walls to corners (used when connecting or disconnection to host) */
  public void createWall(String name1, String name2, String name3, String name4) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3) || this.wall[i].name.equals(name4)) {
        this.wall[i].alive = true;
      }
    }
  }
  
  /* Delete wall from a corner */
  public void deleteWall(String name) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 2 walls from corners (used when connecting or disconnection to host) */
  public void deleteWall(String name1, String name2) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 3 walls from corners (used when connecting or disconnection to host) */
  public void deleteWall(String name1, String name2, String name3) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* Delete 4 walls from corners (used when connecting or disconnection to host) */
  public void deleteWall(String name1, String name2, String name3, String name4) {
    for(int i = 0; i < this.wall.length; i++) {
      if (this.wall[i].name.equals(name1) || this.wall[i].name.equals(name2) || this.wall[i].name.equals(name3) || this.wall[i].name.equals(name4)) {
        this.wall[i].alive = false;
      }
    }
  }
  
  /* set entity to window (used when entity travels between devices) */
  public void updateEntity(String name, boolean alive, int xspd, int yspd, int xsize, int ysize, String direction, String current_device, int xpos, int ypos){
    for(int i = 0; i < this.entity.length; i++) {
      if (this.entity[i] != null && this.entity[i].name.equals(name)) {
        this.entity[i].alive = alive;
        this.entity[i].xpos = xpos;
        this.entity[i].ypos = ypos;
        this.entity[i].xspd = xspd;
        this.entity[i].yspd = yspd;
        this.entity[i].xsize = xsize;
        this.entity[i].ysize = ysize;
        this.entity[i].direction = direction;
        //if (
        //    (xpos <= xsize / 2 && xspd < 0) 
        //    || (xpos >= xsize / 2 && xspd > 0)
        //    || (ypos <= ysize / 2 && yspd < 0)
        //    || (ypos >= ysize / 2 && yspd > 0)
        //    ) {println(1);
          this.entity[i].current_device = current_device;
        //}
        //else {println(2);
        //  this.entity[i].current_device = current_device;
        //}
      }
    }
  }
  
  
  public void checkCell(String direction, String cells, String current_device) {
    //println("mpika server");
    if (this.neighbourCells != null) {
      String[] splited_cells = cells.split("\\|");
      int x_length = this.neighbourCells[0].length,
          y_length = this.neighbourCells[0][0].length;
      
      for (int x = 0; x < x_length; x++) {
        String[] cell_cols = splited_cells[x].split(",");
        for (int y = 0; y < y_length; y++) {
          if (direction.equals("top")) {
            this.neighbourCells[0][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("down")) {
            this.neighbourCells[1][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("left")) {
            this.neighbourCells[2][x][y] = Boolean.valueOf(cell_cols[y]);
          }
          else if (direction.equals("right")) {
            this.neighbourCells[3][x][y] = Boolean.valueOf(cell_cols[y]);
          }
        }

        /*String[] cell_col1 = splited_cells[0].split(",");
        String[] cell_col2 = splited_cells[1].split(",");
        
        if (direction.equals("top")) {
          this.neighbourCells[0] = Arrays.copyOf(cell_col1, cell_col1.length, Boolean[].class);
          this.neighbourCells[1] = Arrays.copyOf(cell_col2, cell_col2.length, Boolean[].class);
        }
        else if (direction.equals("bottom")) {
          this.neighbourCells[x_length-2] = Arrays.copyOf(cell_col1, cell_col1.length, Boolean[].class);
          this.neighbourCells[x_length-1] = Arrays.copyOf(cell_col2, cell_col2.length, Boolean[].class);
        }
        else {
          for (int y = 0; y < y_length; y++) {
            if ( (y > 1 && y < y_length-2) && (x > 1 && x < x_length-2) ) {
              //this.neighbourCells[x][y] = false;
            }
            
            if (direction.equals("left") && x <= 1) {
              if (x == 0) {
                this.neighbourCells[0][y] = Boolean.valueOf(cell_col1[y]);
              }
              else {
                this.neighbourCells[1][y] = Boolean.valueOf(cell_col2[y]);
              }
            }
            else if (direction.equals("right") && x >= x_length-2) {
              if (x == x_length-2) {
                this.neighbourCells[x_length-2][y] = Boolean.valueOf(cell_col1[y]);
              }
              else {
                this.neighbourCells[x_length-1][y] = Boolean.valueOf(cell_col2[y]);
              }
            }
          }
        }*/
      }
      
      // for (int x = 0; x < this.neighbourCells[0].length; x++) {
      //   for (int y = 0; y < this.neighbourCells[0][0].length; y++) {
      //     if (this.neighbourCells[3][x][y] != null)
      //       print(!this.neighbourCells[3][x][y] ? "F" : "T");
      //     else
      //       print("N");
      //   }
      //   println();
      // }


      // for (int y = 0; y < this.neighbourCells.length; y++) {
      //   for (int x = 0; x < this.neighbourCells[0].length; x++) {
      //     if (this.neighbourCells[y][x] != null)
      //       print(!this.neighbourCells[y][x] ? "F" : "T");
      //     else
      //       print("N");
      //   }
      //   println();
      // }
      
    }
  }
  
  public void draw() {
    background(255);
    if(this.entity[0] != null && this.entity[0].isAlive()) {
      this.entity[0].draw();
    }
  }
  
  public void update() {
    this.entity[0].update();
    this.entity[0].screenOut();
  }

  public void keyPressed() {}

  public void mousePressed() {}
  
  public void mouseDragged() {}
}
class Wall extends Entity {
  int fill;
  
  Wall(int _width, int _height, int x, int y) {
    this.xsize = _width;
    this.ysize = _height;
    this.xpos = x;
    this.ypos = y;
  }
  
  public void setColor(int fill) {
    this.fill = fill;
  }
  
  public void update() {

  }
  
  public void draw() {
    if (this.isAlive()) {
      rectMode(CENTER);
      fill(this.fill);
      noStroke();
      rect(this.xpos, this.ypos, this.xsize, this.ysize);
    }
  }

}
class Life extends Simulation {
  // Size of cells
  int cellSize = 5;

  // How likely for a cell to be alive at start (in percentage)
  float probabilityOfAliveAtStart = 15;
  
  // Variables for timer
  int interval = 100;
  int lastRecordedTime = 0;
  
  // Colors for active/inactive cells
  int alive = color(0, 200, 0);
  int dead = color(0);
  
  // Buffer to record the state of the cells and use this while changing the others in the interations
  Entity[][] cellsBuffer; 
  
  Life(int max_entities, Entity[] entities) {
    super(max_entities);
    for (int i = 0; i < entities.length; i++) {
      this.setEntity(entities[i], i);
      this.entity[i].alive = false;
    }
    
    this.cellSize = this.entity[0].xsize;
    
    //this.setupWalls();
    
    this.cells = new Entity[width / this.cellSize][height / this.cellSize];
    this.cellsBuffer = new Entity[width / this.cellSize][height / this.cellSize];
    this.neighbourCells = new Boolean[4][width / this.cellSize][height / this.cellSize];
    
    // This stroke will draw the background grid
    stroke(48);
    
    // Initialization of cells
    int i = 0;
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        float state = random(100);
        if (state > this.probabilityOfAliveAtStart) { 
          state = 0;
        }
        else {
          state = 1;
        }
        cells[x][y] = this.entity[i];
        cellsBuffer[x][y] = new Cell(this.entity[i].xpos, this.entity[i].ypos, this.entity[i].xsize, this.entity[i].current_device);
        cells[x][y].alive = PApplet.parseInt(state) > 0 ? true : false; // Save state of each cell
        i++;
      }
    }
    background(0); // Fill in black in case cells don't cover all the windows
  }
  
  public void iteration() { // When the clock ticks
    // Save cells to buffer (so we opeate with one array keeping the other intact)
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        cellsBuffer[x][y].alive = cells[x][y].alive;
      }
    }
  
    // Visit each cell:
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        // And visit all the neighbours of each cell
        int neighbours = 0; // We'll count the neighbours
        for (int xx = x-1; xx <= x+1; xx++) {
          for (int yy = y-1; yy <= y+1; yy++) {
            
            // TOP Neighbour
            if (xx >= 0 && xx < width / this.cellSize && yy == 0) {
              try {
                Boolean toCheck = neighbourCells[0][xx][(height / this.cellSize)-1];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // BOTTOM Neighbour
            if (xx >= 0 && xx < width / this.cellSize && yy == height / this.cellSize) {
              try {
                Boolean toCheck = neighbourCells[0][xx][0];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // LEFT Neighbour
            if (xx == 0 && yy > 0 && yy < height / this.cellSize) {
              try {
                Boolean toCheck = neighbourCells[2][(width / this.cellSize)-1][yy];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            // RIGHT Neighbour
            if (xx == width / this.cellSize && yy >= 0 && yy < (height / this.cellSize)) {
              try {
                Boolean toCheck = neighbourCells[3][0][yy];
                if (toCheck != null && toCheck) {
                  neighbours ++;
                }
              }
              catch(Exception e) {
                println("exception: " + e.getCause());
              }
            }

            

            if (((xx >= 0) && (xx < width / this.cellSize)) && ((yy >=0 ) && (yy < height / this.cellSize))) { // Make sure you are not out of bounds
              if (!((xx == x) && (yy == y))) { // Make sure to to check against self
                if (cellsBuffer[xx][yy].alive == true){
                  neighbours ++; // Check alive neighbours and count them
                }
              } // End of if
            } // End of if
          } // End of yy loop
        } //End of xx loop
        // We've checked the neigbours: apply rules!
        if (cellsBuffer[x][y].alive == true) { // The cell is alive: kill it if necessary
          if (neighbours < 2 || neighbours > 3) {
            cells[x][y].alive = false; // Die unless it has 2 or 3 neighbours
          }
        } 
        else { // The cell is dead: make it live if necessary      
          if (neighbours == 3 ) {
            cells[x][y].alive = true; // Only if it has 3 neighbours
          }
        } // End of if
      } // End of y loop
    } // End of x loop
  } // End of function
  
  
  public void draw() {
    for (int x = 0; x < width / this.cellSize; x++) {
      for (int y = 0; y < height / this.cellSize; y++) {
        if (cells[x][y].alive == true) {
          fill(alive); // If alive
        }
        else {
          fill(dead); // If dead
        }
        rect (x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);
      }
    }
  }
  
  public void update() {
    // Iterate if timer ticks
    if (millis() - this.lastRecordedTime>interval) {
      this.iteration();
      this.lastRecordedTime = millis();
      this.neighbourCells = new Boolean[4][width / this.cellSize][height / this.cellSize];
    }
  }
  

  public void keyPressed() {
    if (key=='r' || key == 'R') {
      // Restart: reinitialization of cells
      for (int x = 0; x < width / this.cellSize; x++) {
        for (int y = 0; y < height / this.cellSize; y++) {
          float state = random(100);
          if (state > probabilityOfAliveAtStart) {
            state = 0;
          }
          else {
            state = 1;
          }
          this.cells[x][y].alive = PApplet.parseInt(state) > 0 ? true : false;
        }
      }
    }
  }

}
class Maze extends Simulation {
  Maze(int max_entities) {
    super(max_entities);
  }
  
  public void draw() {
    
  }
  
  public void update() {
    
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "server_rpi" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
